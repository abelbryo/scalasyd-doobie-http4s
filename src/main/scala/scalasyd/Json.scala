package scalasyd

import argonaut._
import Argonaut._
import org.joda.time.LocalDate
import org.joda.time.DateTimeZone
import scalaz.stream._

object Json {

  def codec[A: EncodeJson: DecodeJson] = CodecJson.derived[A]

  implicit val artistIdJson: CodecJson[ArtistId] = codec[Long].xmap(ArtistId(_))(_.id)
  implicit val artistJson: CodecJson[Artist] = codec[String].xmap(Artist(_))(_.name)
  implicit val bandIdJson: CodecJson[BandId] = codec[Long].xmap(BandId(_))(_.id)
  implicit val localDateJson: CodecJson[LocalDate] =
    codec[Long].xmap(new LocalDate(_))(_.toDateTimeAtStartOfDay(DateTimeZone.UTC).getMillis)
  implicit val bandJson: CodecJson[Band] = casecodec2(Band.apply, Band.unapply)("name", "started")

  implicit val artistViewJson: CodecJson[ArtistView] = casecodec2(ArtistView.apply, ArtistView.unapply)("id", "name")
  implicit val bandViewJson: CodecJson[BandView] = casecodec2(BandView.apply, BandView.unapply)("id", "band")
  implicit val bandArtistViewJson: CodecJson[BandArtistView] =
    casecodec2(BandArtistView.apply, BandArtistView.unapply)("band", "artists")

  implicit class AddJsonToProcess[M[_], A: CodecJson](as: Process[M, A]) {
    import Process._

    def asJsonArray: Process[M, String] =
      emit("[") ++ as.map(_.asJson.nospaces).intersperse(",") ++ emit("]")
  }
}