package scalasyd

import org.http4s.server.HttpService
import org.http4s.server.blaze.BlazeBuilder
import org.http4s.dsl._
import org.http4s.argonaut._
import org.http4s.EntityDecoder
import doobie.imports._
import argonaut._
import Argonaut._
import Json._

object Web {

  implicit val bandDecoder: EntityDecoder[Band] = jsonOf[Band]

  val read = HttpService {
    case GET -> Root / "bands" / "artists" =>
      Ok {
        val a = Database.findBandsWithArtists.transact(Database.tx)
        a.asJsonArray
      }
  }

  val write = HttpService {
    case req @ PUT -> Root / "band" =>
      req.decode[Band] { b =>
        Ok {
          Database.createBand(b).transact(Database.tx).map(_.asJson.nospaces)
        }
      }
  }

  val service: HttpService = read orElse write

  def main(args: Array[String]): Unit =
    BlazeBuilder.bindHttp(8080)
      .mountService(service, "/")
      .run
      .awaitShutdown()

}

